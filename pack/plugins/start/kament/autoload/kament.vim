" Vim plugin
" Name: kament
" License: ISC
" Maintainer:	Steve
" Last Change:	2020 Feb 16

" Checks <...>kament/ftplugin directory for existence of detected filetypes *.vim, returns 1 on success.
function! g:kament#FtFileExists()
  if exists('g:kament#slkament')
    return 1
  endif
  echom "Err: no file defines comments for filetype " . &ft . ", add one in <...>pack/plugins/start/kament/ftplugin."
  return 0
endfunction

" Determine the minimum indent for multiple lines.
function! g:kament#CheckMinIndent(start, end)
  let l:min_indent = -1
  let l:indent = a:start
  while l:indent <= a:end
    if l:min_indent == -1 || indent(l:indent) < l:min_indent
      let l:min_indent = indent(l:indent)
    endif
    let l:indent += 1
  endwhile
  return l:min_indent
endfunction

function! g:kament#ProcessLines(lnum, line, indent, add_comment)
  " If there is no indent.
  let l:prefix = a:indent > 0 ? a:line[:a:indent - 1] : ''
  if a:add_comment
    call setline(a:lnum, l:prefix . g:kament#slkament . a:line[a:indent:])
  else
    call setline(a:lnum, l:prefix . a:line[a:indent + len(g:kament#slkament):])
  endif
endfunction


" Toggle comments on selected line(s).
function! g:kament#Ctoggle(count)
  if !g:kament#FtFileExists()
    return
  endif
  let l:start = line('.')
  let l:end = l:start + a:count - 1
  if l:end > line('$') " $ == EOF
    let l:end = line('$')
  endif
  let l:indent = g:kament#CheckMinIndent(l:start, l:end)
  let l:lines = getline(l:start, l:end)
  let l:cur_row = getcurpos()[1]
  let l:cur_col = getcurpos()[2]
  let l:lnum = l:start
  if l:lines[0][l:indent:l:indent + len(g:kament#slkament) - 1] ==# g:kament#slkament
    let l:add_comment = 0
    let l:cur_offset = -len(g:kament#slkament)
  else
    let l:add_comment = 1
    let l:cur_offset = len(g:kament#slkament)
  endif
  for l:line in l:lines
    call g:kament#ProcessLines(l:lnum, l:line, l:indent, l:add_comment)
    let l:lnum += 1
  endfor
  call cursor(l:cur_row, l:cur_col + l:cur_offset)
endfunction

