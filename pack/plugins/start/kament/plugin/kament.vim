" Vim plugin
" Name: kament
" License: ISC
" Maintainer:	Steve
" Last Change:	2020 Feb 16

nnoremap <silent> <Leader>c :<C-U>call g:kament#Ctoggle(v:count1)<CR>
vnoremap <silent> <Leader>c :<C-U>call g:kament#Ctoggle(line("'>") - line("'<") + 1)<CR>

