# Vim kament plugin

Vim plugin that **comments** and **uncomments** lines of code using single-line language specific comments.

Installation:

copy pack directory with it's contents into:

**Win**:

%USERPROFILE%\\vimfiles\\

**+nix**:

$HOME/.vim/

In **vim** enter:

:packloadall

:helptags ALL

Commenting single line in text files. Filetype is detected by built-in
vim's ftplugin. Supports different comments for different *file types*:
*c, javascript, lua, make, python, sh, text, vb, vim*

Add vim script for desired filetype into:

**Win**:

%USERPROFILE%\\vimfiles\\pack\\plugins\\start\\kament\\ftplugin\\

**+nix**:

$HOME/.vim/pack/plugins/start/kament/ftplugin/

Usage:

Key bindings:

\<Leader>c: toggle single comment on a current line

To set leader key add these lines to vimrc:

noremap \<Space> \<Nop>

let mapleader = \"\\\<Space>\"

